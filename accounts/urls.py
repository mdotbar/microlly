from django.conf.urls import url
from . import views

app_name = 'accounts'

urlpatterns = [
    url(r'^signup/$', views.signup_view, name="signup"), # url de la page de création    mb#
    url(r'^login/$', views.login_view, name="login"),    # url de la page de connexion   mb#
    url(r'^logout/$', views.logout_view, name="logout"), # url de la déconnexion         mb#
]
