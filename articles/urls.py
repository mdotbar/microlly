from django.conf.urls import url
from . import views

app_name = 'articles'

urlpatterns = [
    url(r'^$', views.article_list, name="list"),                      # url de la liste d'articles    mb#
    url(r'^create/$', views.article_create, name="create"),           # url de la création d'article  mb#
    url(r'^(?P<slug>[\w-]+)/$', views.article_detail, name="detail"), # url d'un article avec le slug mb#
]